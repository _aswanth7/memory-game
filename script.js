const enterBtn = document.querySelector("#enterBtn");
const startBtn = document.querySelector("#startBtn");
const restartBtn = document.querySelector("#restartBtn");
const modalClose = document.querySelector("#modalClose");
const modalClose2 = document.querySelector("#modalClose2");
const restartYes = document.querySelector("#restartYes");
const restartNo = document.querySelector("#restartNo");
const restart = document.querySelector("#restart");
const scoreBoardBtn = document.querySelector("#scoreBoard");
const userNameBtn = document.querySelector("#userNameBtn");
const emailBtn = document.querySelector("#emailBtn");

const level = document.querySelector("#level");
const levelSpan = document.querySelector("#levelSpan");
const username = document.querySelector("#name");
const email = document.querySelector("#email");
const nameInfo = document.querySelector("#infoMsg-1");
const mailInfo = document.querySelector("#infoMsg-2");
const levelInfo = document.querySelector("#infoMsg-3");

const gameStart = document.querySelector("#gameStart");
const gameLevel = document.querySelector("#gameLevel");
const gamePlay = document.querySelector("#gamePlay");
const gameUserName = document.querySelector("#gameUserName");
const gameUserEmail = document.querySelector("#gameUserEmail");
const gameHighScore = document.querySelector("#gameHighScore");
const gameHighScoreTable = document.querySelector("#gameHighScore table");
const modal = document.querySelector("#modal");
const modal2 = document.querySelector("#modal-2");
const modal3 = document.querySelector("#modal-3");
const gameContainer = document.querySelector("#game");
const score = document.querySelector("#score");
const curScore = document.querySelector("#curscore");
const loader = document.querySelector("#loader");
const successMsg = document.querySelector("#modal-2 h3");
const newHighScore = document.querySelector("#modal-2 h4");
const mailNotification = document.querySelector("#modal-3 h2");

let clickedDiv = [];
let topScore = [];
let matchCount = 0;
let clickCount = 0;
const IMAGE = [];
const GIF = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
];

function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

function createDivsForImage(shuffledImage) {
  for (let image of shuffledImage) {
    const newDiv = document.createElement("div");

    newDiv.setAttribute("data-imgUrl", image);

    newDiv.classList.add("shake");

    newDiv.addEventListener("click", handleCardClick);

    gameContainer.append(newDiv);
  }
}

const createImage = async () => {
  let numberOfImage = parseInt(level.value);
  try {
    loader.style.visibility = "visible";

    for (let number = 0; number < numberOfImage; number++) {
      let image = await fetch(
        "https://dog.ceo/api/breeds/image/random"
      ).then((response) => response.json());
      IMAGE.push(image.message);
      IMAGE.push(image.message);
    }
  } catch (error) {
    console.log(error);
    for (let index = 0; index < numberOfImage; index++) {
      IMAGE.push(GIF[index]);
      IMAGE.push(GIF[index]);
    }
  }
};
function showTopScore() {
  getTopScore().then(() => {
    for (scores of topScore) {
      let newRow = document.createElement("tr");
      let usernameTD = document.createElement("td");
      usernameTD.innerText = scores.username;
      let usescoreTD = document.createElement("td");
      usescoreTD.innerText = scores.score;
      let usermailTD = document.createElement("td");
      usermailTD.innerText = scores.usermail;
      let dateTD = document.createElement("td");
      dateTD.innerText = scores.date;

      newRow.append(usernameTD, usescoreTD, usermailTD, dateTD);
      gameHighScoreTable.appendChild(newRow);
    }
  });
}

getTopScore = async () => {
  try {
    topScore = await fetch(
      `https://memeory-game-backend.herokuapp.com/api/topscore/${level.value}`
    ).then((response) => response.json());
    console.log(topScore);
  } catch (error) {
    console.log(error);
  }
};

const sendMail = async (score) => {
  try {
    const message = {
      username: username.value,
      usermail: email.value,
      level: level.value,
      score,
    };
    console.log(JSON.stringify(message));
    let sendMailResponse = await fetch(
      "https://memeory-game-backend.herokuapp.com/api/sendmail",
      {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-Type": "Application/json",
        },
        body: JSON.stringify(message),
      }
    ).then((response) => response.json());
    console.log(sendMailResponse);
    if (sendMailResponse.result == "Success") {
      modal3.style.display = "block";
      mailNotification.style.color = "green";
      setTimeout(() => {
        modal3.style.display = "none";
      }, 2000);
    } else {
      mailNotification.innerText = "Mail sending failed!";
      mailNotification.style.color = "red";
      modal3.style.display = "block";
      setTimeout(() => {
        modal3.style.display = "none";
      }, 2000);
    }
  } catch (error) {
    console.log(error);
    mailNotification.innerText = "Mail sending failed!";
    mailNotification.style.color = "red";
    modal3.style.display = "block";
    setTimeout(() => {
      modal3.style.display = "none";
    }, 2000);
  }
};

function handleCardClick(event) {
  clickCount += 1;
  curScore.innerText = clickCount;
  let imgUrl = event.target.getAttribute("data-imgUrl");
  event.target.style.backgroundImage = `url(${imgUrl})`;
  event.target.style.backgroundRepeat = "no-repeat";
  event.target.style.backgroundSize = "cover";
  event.target.style.backgroundPosition = "center";

  if (clickedDiv.length < 1) {
    clickedDiv.push(event.target);
    clickedDiv[0].removeEventListener("click", handleCardClick);
  } else {
    if (
      clickedDiv[0].getAttribute("data-imgUrl") ==
      event.target.getAttribute("data-imgUrl")
    ) {
      console.log("Got Match");
      matchCount += 1;
      if (matchCount == IMAGE.length / 2) {
        let topPlayerScore = topScore[0].score;
        sendMail(clickCount);
        modal2.style.display = "block";

        successMsg.innerText = `Your Score is ${clickCount}`;
        if (clickCount < topPlayerScore) {
          localStorage.setItem(level.value, clickCount);
          newHighScore.innerText = "New High Score";
        }
      }
      clickedDiv[0].removeEventListener("click", handleCardClick);
      event.target.removeEventListener("click", handleCardClick);
    } else {
      clickedDiv[0].addEventListener("click", handleCardClick);
      console.log("Flip Card");
      gameContainer.style.pointerEvents = "none";

      clickedDiv[0].style.animation = "shake 1s  linear";
      event.target.style.animation = "shake 1s  linear";
      setTimeout(
        function (clickedDiv) {
          clickedDiv[0].style.animation = "none";
          event.target.style.animation = "none";
          clickedDiv[0].style.background =
            "linear-gradient(315deg, #fe5858 0%, #ee9617 74%)";
          event.target.style.background =
            "linear-gradient(315deg, #fe5858 0%, #ee9617 74%)";
          gameContainer.style.pointerEvents = "auto";
        },
        1500,
        clickedDiv
      );
    }
    clickedDiv = [];
  }
}

// Event Listeners

enterBtn.addEventListener("click", () => {
  gameStart.style.display = "none";
  gameUserName.style.display = "flex";
});
userNameBtn.addEventListener("click", () => {
  let userName = username.value;
  if (userName == "") {
    nameInfo.innerText = "Enter a valid User Name";
  } else {
    gameUserName.style.display = "none";
    gameUserEmail.style.display = "flex";
  }
});
emailBtn.addEventListener("click", () => {
  let userEmail = email.value;
  let mailReg = /^.+@.+\..+$/;
  if (userEmail == "" || !mailReg.test(userEmail)) {
    mailInfo.innerText = "Enter a valid Email";
  } else {
    gameUserEmail.style.display = "none";
    gameLevel.style.display = "flex";
  }
});

startBtn.addEventListener("click", () => {
  let selectedLevel = parseInt(level.value);
  console.log(selectedLevel, level.value);

  if (isNaN(selectedLevel) || selectedLevel < 3 || selectedLevel > 10) {
    levelInfo.innerText = "Enter a valid level";
  } else {
    gamePlay.style.display = "flex";
    gameLevel.style.display = "none";
    levelSpan.innerText = level.value;
    createImage()
      .then(() => {
        return getTopScore(level.value);
      })
      .then(() => {
        loader.style.visibility = "hidden";
        let topPlayer = topScore[0] != undefined ? topScore[0].score : "0";

        score.innerText = topPlayer;
        let shuffledImage = shuffle(IMAGE);
        gameContainer.style.display = "gird";
        createDivsForImage(shuffledImage);
      })
      .catch((err) => {
        console.log(err, "here");
      });
  }
});
restart.addEventListener("click", () => {
  location.reload();
  console.log(modalClose2);
});
scoreBoardBtn.addEventListener("click", () => {
  showTopScore();
  gameHighScore.style.display = "flex";
  modal2.style.display = "none";
});
modalClose2.addEventListener("click", () => {
  gamePlay.style.display = "flex";
  modal2.style.display = "none";
});
restartBtn.addEventListener("click", () => {
  modal.style.display = "block";
});
modalClose.addEventListener("click", () => {
  gamePlay.style.display = "flex";
  modal.style.display = "none";
});

restartNo.addEventListener("click", () => {
  gamePlay.style.display = "flex";
  modal.style.display = "none";
});
restartYes.addEventListener("click", () => {
  location.reload();
});
